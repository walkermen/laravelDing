<?php
namespace DingDing\Laravel\Tools;

use Illuminate\Config\Repository;

class DingDingTools
{
    protected $config;

    public function __construct(Repository $config)
    {
        $this->config = $config->get('dingding');
    }

    // text
    public function send($type, $text, $phone=[])
    {
        //
        $accessToken = $this->config['token'][$type];
        $host = $this->config['host'];
        $uri  = $this->config['url']['sendMessage'];
        $url = "{$host}{$uri}?access_token={$accessToken}";

        if (empty($accessToken) || empty($host) || empty($uri)) {
            throw new \Exception('Dingding 缺少参数信息');
        }

        $data = array (
            'msgtype' => 'text',
            'text' => [
                'content' => (string)$text
            ],
            'at' => [
                'atMobiles' => $phone,
                "isAtAll" => false
            ]
        );

        $data_string = json_encode($data);
        $headers = array(
            'Content-Type: application/json;charset=utf-8'
        );
        $result = $this->curlRequest($url,'POST',$headers,$data_string);
        return $result;
    }

    protected function curlRequest($url, $method, $headers, $params)
    {
        if (is_array($params)) {
            $requestString = http_build_query($params);
        } else {
        $requestString = $params ? : '';
        }
        if (empty($headers)) {
            $headers = array('Content-Type: application/json;charset=utf-8');
        } elseif (!is_array($headers)) {
            parse_str($headers,$headers);
        }
        // setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        // setting the POST FIELD to curl
        switch ($method){
            case "GET" : curl_setopt($ch, CURLOPT_HTTPGET, 1);break;
            case "POST": curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestString);break;
            case "PUT" : curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestString);break;
            case "DELETE":  curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestString);break;
        }
        // getting response from server
        $response = curl_exec($ch);

        //close the connection
        curl_close($ch);

        return $response;
    }
}
