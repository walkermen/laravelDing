<?php

namespace DingDing\Laravel;

use Illuminate\Support\ServiceProvider;

class DingDingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('DingDing',function ($app){
            return new \DingDing\Laravel\Tools\DingDingTools($app['config']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__.'/config/dingding.php' => config_path('dingding.php'),
        ]);
    }
}
